<div class="container">	
    <ul class="timeline">
        <li>
            <div class="timeline-badge primary"><i class="fa fa-image"></i></div>
            <div class="timeline-panel wow animated fadeInUp">
                <div class="timeline-heading">
                    <img class="img-responsive" src="img/img-6.jpg" alt=""/>
                </div>

                <div class="timeline-body">
                    <h2><a href="#">Standard image post</a></h2>
                    <ul class="list-inline post-detail">
                        <li>by <a href="#">assan</a></li>
                        <li><i class="fa fa-calendar"></i> 31st july 2014</li>                            
                    </ul>
                    <p>Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    <a class="btn border-theme" href="#">Read More</a>
                </div>

            </div>
        </li>
        <li class="timeline-inverted">
            <div class="timeline-badge primary"><i class="fa fa-image invert"></i></div>
            <div class="timeline-panel wow animated fadeInUp">
                <div class="timeline-heading">
                    <img class="img-responsive" src="img/img-5.jpg" alt=""/>
                </div>
                <div class="timeline-body">
                    <h2><a href="#">Standard image post</a></h2>
                    <ul class="list-inline post-detail">
                        <li>by <a href="#">assan</a></li>
                        <li><i class="fa fa-calendar"></i> 31st july 2014</li>                            
                    </ul>
                    <p>Lorem Ipsum is simply dummy text of the printing and type setting industry.printing and type setting industry Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                    </p>

                    <a class="btn border-theme" href="#">Read More</a>
                </div>

            </div>
        </li>
        
        
        <li class="clearfix" style="float: none;"></li>
    </ul>
    </div>